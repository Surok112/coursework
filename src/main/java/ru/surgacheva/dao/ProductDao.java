package ru.surgacheva.dao;

import javax.ejb.Stateless;
import ru.surgacheva.model.Product;

@Stateless
public class ProductDao extends WorkBD<Product, Long>{

    public ProductDao() {
        super(Product.class);
    }
}
