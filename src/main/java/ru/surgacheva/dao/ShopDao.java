package ru.surgacheva.dao;

import javax.ejb.Stateless;
import ru.surgacheva.model.Shop;

@Stateless
public class ShopDao extends WorkBD<Shop, Long>{

    public ShopDao() {
        super(Shop.class);
    }
}
