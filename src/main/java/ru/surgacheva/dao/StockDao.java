package ru.surgacheva.dao;

import javax.ejb.Stateless;
import ru.surgacheva.model.Stock;

@Stateless
public class StockDao extends WorkBD<Stock, Long>{

    public StockDao() {
        super(Stock.class);
    }
}
