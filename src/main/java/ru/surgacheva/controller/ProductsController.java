package ru.surgacheva.controller;

import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import ru.surgacheva.model.Product;
import ru.surgacheva.sevice.ProductService;
import java.util.ArrayList;
import java.util.Arrays;


@Named(value = "products")
@RequestScoped
public class ProductsController {

    @EJB
    private ProductService productService;

    @Inject
    private UserController userController;

    private void addMessage(String summary, String detail) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public List<Product> getProducts() {
        return productService.getProducts();
    }
    
//    public List<OperationType> getOperationTypes() {
//        return new ArrayList<>(Arrays.asList(OperationType.values()));
//    }

    public void createProduct() {
        if (userController.getSelectedProduct() == null) {
            addMessage("Добавление товара", "Ошибка добавления товара");
            return;
        }
        try {
            productService.createProduct(userController.getSelectedProduct());
            addMessage("Добавление товара", "Товар успешно добавлен");
        } catch (Exception e) {
            addMessage("Добавление товара", String.format("Ошибка добавления товара %n %s", e.getMessage()));
        }
    }

    public void updateProduct() {
        if (userController.getSelectedProduct() == null) {
            addMessage("Изменение товара", "Ошибка изменения товара");
            return;
        }
        try {
            productService.editProduct(userController.getSelectedProduct());
            addMessage("Изменение товара", "Товар успешно изменён");
        } catch (Exception e) {
            addMessage("Изменение товара", String.format("Ошибка изменения товара %n %s", e.getMessage()));
        }
    }

    public void deleteProduct() {
        if (userController.getSelectedProduct() == null) {
            addMessage("Удаление товара", "Ошибка удаления товара");
            return;
        }

        try {
            productService.deleteProduct(userController.getSelectedProduct());
            addMessage("Удаление товара", "Товар успешно удален");
            userController.deleteProductObject();
        } catch (Exception e) {
            addMessage("Удаление товара", String.format("Ошибка удаления товара %n %s", e.getMessage()));
        }
    }
}
