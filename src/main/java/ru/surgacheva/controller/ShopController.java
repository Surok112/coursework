package ru.surgacheva.controller;

import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import ru.surgacheva.model.Shop;
import ru.surgacheva.sevice.ShopService;

@Named(value = "shop")
@RequestScoped
public class ShopController {

    @EJB
    private ShopService shopService;

    @Inject
    private UserController userController;

    private void addMessage(String summary, String detail) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public List<Shop> getShop() {
        return shopService.getShop();
    }

    public void createShop() {
        if (userController.getSelectedShop() == null) {
            addMessage("Добавление магазина", "Ошибка добавления магазина");
            return;
        }
        try {
            shopService.createShop(userController.getSelectedShop());
            addMessage("Добавление магазина", "Магазин успешно добавлен");
        } catch (Exception e) {
            addMessage("Добавление магазина", String.format("Ошибка добавления магазина %n %s", e.getMessage()));
        }
    }

    public void updateShop() {
        if (userController.getSelectedShop() == null) {
            addMessage("Изменение магазина", "Ошибка изменения магазина");
            return;
        }
        try {
            shopService.editShop(userController.getSelectedShop());
            addMessage("Изменение магазина", "Магазин успешно изменён");
        } catch (Exception e) {
            addMessage("Изменение магазина", String.format("Ошибка изменения магазина %n %s", e.getMessage()));
        }
    }

    public void deleteShop() {
        if (userController.getSelectedShop() == null) {
            addMessage("Удаление магазина", "Ошибка удаления магазина");
            return;
        }

        try {
            shopService.deleteShop(userController.getSelectedShop());
            addMessage("Удаление магазина", "Магазин успешно магазина");
            userController.deleteShopObject();
        } catch (Exception e) {
            addMessage("Удаление магазина", String.format("Ошибка удаления магазина %n %s", e.getMessage()));
        }
    }
}
