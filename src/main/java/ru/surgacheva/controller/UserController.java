package ru.surgacheva.controller;

import java.io.Serializable;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
//import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import ru.surgacheva.model.Product;
import ru.surgacheva.model.Shop;

import ru.surgacheva.model.Stock;
//import ru.surgacheva.model.User;
//import ru.surgacheva.sevice.UserService;

@Named(value = "userSession")
@SessionScoped
public class UserController implements Serializable {

   // @EJB
  //  private UserService userService;

   // private User user;

 //   private User selectedUser;

  

    private Product selectedProduct;
    
    private Shop selectedShop;
    
    private Stock selectedStock;

  // private OperationType selectedDeliveryNote;


    
    // >> Products
    public Product getSelectedProduct() {
        return selectedProduct;
    }

    public void setSelectedProduct(Product selectedProduct) {
        this.selectedProduct = selectedProduct;
    }
    
    public void createProductObject() {
        selectedProduct = new Product();
    }
    
    public void deleteProductObject() {
        selectedProduct = null;
    }
    

    
    
    
    // >> Shop
    public Shop getSelectedShop() {
        return selectedShop;
    }

    public void setSelectedShop(Shop selectedShop) {
        this.selectedShop = selectedShop;
    }
    
    public void createShopObject() {
        selectedShop = new Shop();
    }
    
    public void deleteShopObject() {
        selectedShop = null;
    }
    
    // >> Stock
    public Stock getSelectedStock() {
        return selectedStock;
    }

    public void setSelectedStock(Stock selectedStock) {
        this.selectedStock = selectedStock;
    }
    
    public void createStockObject() {
        selectedStock = new Stock();
    }
    
    public void deleteStockObject() {
        selectedStock = null;
    }
   
   
}
