package ru.surgacheva.controller;

import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import ru.surgacheva.model.Stock;
import ru.surgacheva.sevice.StockService;

@Named(value = "stock")
@RequestScoped
public class StockController {

    @EJB
    private StockService stockService;

    @Inject
    private UserController userController;

    private void addMessage(String summary, String detail) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public List<Stock> getStock() {
        return stockService.getStock();
    }

    public void createStock() {
        if (userController.getSelectedStock() == null) {
            addMessage("Добавление склада", "Ошибка добавления склада");
            return;
        }
        try {
            stockService.createStock(userController.getSelectedStock());
            addMessage("Добавление склада", "Склад успешно добавлен");
        } catch (Exception e) {
            addMessage("Добавление склада", String.format("Ошибка добавления склада %n %s", e.getMessage()));
        }
    }

    public void updateStock() {
        if (userController.getSelectedStock() == null) {
            addMessage("Изменение склада", "Ошибка изменения склада");
            return;
        }
        try {
            stockService.editStock(userController.getSelectedStock());
            addMessage("Изменение склада", "Склад успешно изменён");
        } catch (Exception e) {
            addMessage("Изменение склада", String.format("Ошибка изменения склада %n %s", e.getMessage()));
        }
    }

    public void deleteStock() {
        if (userController.getSelectedStock() == null) {
            addMessage("Удаление склада", "Ошибка удаления склада");
            return;
        }

        try {
            stockService.deleteStock(userController.getSelectedStock());
            addMessage("Удаление склада", "Склад успешно удален");
            userController.deleteStockObject();
        } catch (Exception e) {
            addMessage("Удаление склада", String.format("Ошибка удаления склада %n %s", e.getMessage()));
        }
    }
}
