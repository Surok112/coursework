package ru.surgacheva.sevice;

import java.util.List;
import javax.ejb.Local;
import ru.surgacheva.model.Shop;

@Local
public interface ShopService {

    Shop createShop(Shop shop);

    Shop editShop(Shop shop);

    void deleteShop(Shop shop);

    Shop getShopById(Long id);

    List<Shop> getShop();
}
