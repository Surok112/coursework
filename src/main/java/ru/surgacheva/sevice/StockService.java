package ru.surgacheva.sevice;

import java.util.List;
import javax.ejb.Local;
import ru.surgacheva.model.Stock;

@Local
public interface StockService {

    Stock createStock(Stock stock);

    Stock editStock(Stock stock);

    void deleteStock(Stock stock);

    Stock getStockById(Long id);

    List<Stock> getStock();
}
