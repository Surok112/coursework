package ru.surgacheva.sevice;

import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import ru.surgacheva.dao.ProductDao;
import ru.surgacheva.model.Product;

@Stateless
public class ProductServiceImpl implements ProductService {
    
    @EJB
    private ProductDao productDao;

    @Override
    public Product createProduct(Product product) {
        return productDao.create(product);
    }

    @Override
    public Product editProduct(Product product) {
        return productDao.update(product);
    }

    @Override
    public void deleteProduct(Product product) {
        productDao.remove(product);
    }

    @Override
    public Product getProductById(Long id) {
        return productDao.findById(id);
    }

    @Override
    public List<Product> getProducts() {
        return productDao.findAll();
    }
    
}
