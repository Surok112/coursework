package ru.surgacheva.sevice;

import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import ru.surgacheva.dao.ShopDao;
import ru.surgacheva.model.Shop;

@Stateless
public class ShopServiceImpl implements ShopService {
    
    @EJB
    private ShopDao shopDao;

    @Override
    public Shop createShop(Shop shop) {
        return shopDao.create(shop);
    }

    @Override
    public Shop editShop(Shop shop) {
        return shopDao.update(shop);
    }

    @Override
    public void deleteShop(Shop shop) {
        shopDao.remove(shop);
    }

    @Override
    public Shop getShopById(Long id) {
        return shopDao.findById(id);
    }

    @Override
    public List<Shop> getShop() {
        return shopDao.findAll();
    }
    
}
