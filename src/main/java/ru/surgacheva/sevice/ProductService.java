package ru.surgacheva.sevice;

import java.util.List;
import javax.ejb.Local;
import ru.surgacheva.model.Product;

@Local
public interface ProductService {

    Product createProduct(Product product);

    Product editProduct(Product product);

    void deleteProduct(Product product);

    Product getProductById(Long id);

    List<Product> getProducts();
}
