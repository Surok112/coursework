package ru.surgacheva.model;

//import java.util.Objects;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_products")
public class Product {
    
    

      @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    @Column(nullable = false, length = 255)
    private String name;
     
//    @Enumerated(EnumType.STRING)
//    @Column(nullable = false)
//    private OperationType operations;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable
    private List<Shop> shop;
    
     @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
     @JoinTable
     private List<Stock> stock;
     
// public Product() {
//        //shop = new LinkedList<>();
//     
//        //stock = new LinkedList<>();
//        operations = OperationType.ARRIVAL;
//    }
 

//    @Override
//    public boolean equals(Object obj) {
//        if (obj == null) {
//            return false;
//        }
//        if (Objects.equals(Long.class, obj.getClass())) {
//            return Objects.equals(this.id, obj);
//        }
//        if (Objects.equals(this.getClass(), obj.getClass())) {
//            Product objProduct = (Product) obj;
//            return Objects.equals(this.id, objProduct.id);
//        }
//        return false;
//    }

//    @Override
//    public int hashCode() {
//        int hash = 5;
//        hash = 37 * hash + Objects.hashCode(this.id);
//        return hash;
//    }
}
